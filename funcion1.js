function numero_grupos (profesor){
    if (profesor["grupos"] ){
        return profesor ["grupos"].length;
    }
    return 0;
}


obj_profesor1 = {
    "clave" : " 123445",
    "nombre" : "sara Velasco Hernandez",
    "grupos" : [ 
        {"clave" : "1", "nombre" : "a"},
        {"clave" : "2", "nombre" : "b"}

        ]
}

obj_profesor2 = {
    "clave" : " 123445",
    "nombre" : "sara Velasco Hernandez",
    "grupos" : []
}
obj_profesor3 = {
    "clave" : " 123445",
    "nombre" : "sara Velasco Hernandez",
}


console.log (numero_grupos(obj_profesor1));
console.log (numero_grupos(obj_profesor2));
console.log (numero_grupos(obj_profesor3));
